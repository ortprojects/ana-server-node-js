# Apto no Apto

## Development setup
```
npm install
npm run dev
```

## Run test
The files for testing should be created in the <test> folder, separating the testing modules
```
npm test
```
## ORM
This project use Sequelize V5 as ORM, please see oficial documentation to correct usage.

Sequelize V5 - [https://sequelize.org/v5/](https://sequelize.org/v5/)

## Migrations
This project use sequelize-cli to create migrations, please see oficial documentation to correct usage.

Sequelize cli - [https://github.com/sequelize/cli](https://github.com/sequelize/cli) 

## Environments
Create .env file. Is necessary to run app. 
Here an example.
```
API_PATH=api
API_VERSION=v1
PORT=8000
DB_USERNAME=username
DB_PASSWORD=password
DB_DATABASE_NAME=db-name
DB_HOST=db-host
```