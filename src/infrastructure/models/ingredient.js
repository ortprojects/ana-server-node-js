'use strict';
module.exports = (sequelize, DataTypes) => {
  const Ingredient = sequelize.define('Ingredient', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    defaultScope: {
      attributes: { exclude: ['userId', 'ingredientTypeId'] }
    },
    underscored: true,
    tableName: "Ingredients",
  });
  Ingredient.associate = function (models) {
    models.Ingredient.belongsTo(models.User, { as: "user" });
    models.Ingredient.belongsTo(models.IngredientType, { as: "ingredientType" });
  };
  return Ingredient;
};