'use strict';
module.exports = (sequelize, DataTypes) => {
  const HairPattern = sequelize.define(
    'HairPattern',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      underscored: true,
      tableName: 'HairPatterns',
    }
  );
  HairPattern.associate = function (models) {
    models.HairPattern.belongsToMany(models.User, {
      through: models.UserHairPatterns,
      foreignKey: 'hairPatternId',
      onDelete: 'CASCADE',
    });
  };
  return HairPattern;
};
