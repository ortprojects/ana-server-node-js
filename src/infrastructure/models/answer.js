'use strict';
module.exports = (sequelize, DataTypes) => {
  const Answer = sequelize.define('Answer', {
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    postId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Posts',
        key: 'id',
      },
    },
  }, {
    defaultScope: {
      attributes: { exclude: ['userId'] }
    },
    underscored: true,
    tableName: "Answers",
  });
  Answer.associate = function(models) {
    models.Answer.belongsTo(models.User, {
      as: 'user'
    });

    models.Answer.belongsTo(models.Post, {
      as: 'post'
    });
  };
  return Answer;
};