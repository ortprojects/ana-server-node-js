'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      avatarUrl: {
        type: DataTypes.STRING,
      },
      firebaseUid: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      userRolId: {
        type: DataTypes.INTEGER,
        defaultValue: 3, // Registrado
        allowNull: false,
        references: {
          model: 'UserRoles',
          key: 'id',
        },
      }
    },
    {
      defaultScope: {
        attributes: { exclude: ['userRolId'] },
      },
      underscored: true,
      tableName: 'Users',
    }
  );
  User.associate = function (models) {
    models.User.belongsToMany(models.HairType, {
      through: models.UserHairTypes,
      as: 'hairTypes',
      foreignKey: 'userId',
      onDelete: 'CASCADE'
    });

    models.User.belongsToMany(models.HairPattern, {
      through: models.UserHairPatterns,
      as: 'hairPatterns',
      foreignKey: 'userId',
      onDelete: 'CASCADE'
    });

    models.User.belongsTo(models.UserRol, {
      as: 'userRol'
    });

    models.User.hasMany(models.Post);

    models.User.hasMany(models.Answer);
  };
  return User;
};
