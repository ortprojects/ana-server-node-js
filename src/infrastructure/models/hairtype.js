'use strict';
module.exports = (sequelize, DataTypes) => {
  const HairType = sequelize.define('HairType', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    underscored: true,
    tableName: "HairTypes",
  });
  HairType.associate = function(models) {
    models.HairType.belongsToMany(models.User, {
      through: models.UserHairTypes,
      foreignKey: 'hairTypeId',
      onDelete: 'CASCADE'
    });
  };
  return HairType;
};