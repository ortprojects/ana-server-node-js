'use strict';
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    }
  },
  {
    defaultScope: {
      attributes: { exclude: ['userId'] }
    },
    underscored: true,
    tableName: "Posts",
  });
  Post.associate = function(models) {
    models.Post.belongsTo(models.User, {
      as: 'user'
    });

    models.Post.hasMany(models.Answer, {
      as: 'answers'
    });
  };
  return Post;
};
