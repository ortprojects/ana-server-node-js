'use strict';
module.exports = (sequelize, DataTypes) => {
  const IngredientType = sequelize.define('IngredientType', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    underscored: true,
    tableName: "IngredientTypes",
  });
  IngredientType.associate = function(models) {
    // associations can be defined here
  };
  return IngredientType;
};