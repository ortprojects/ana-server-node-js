'use strict';

module.exports = (sequelize, DataTypes) => {
  const UserHairPatterns = sequelize.define(
    'UserHairPatterns',
    {
      userId: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      hairPatternId: {
        type: DataTypes.INTEGER,
        references: {
          model: 'HairPatterns',
          key: 'id',
        },
      }
    },
    {
      underscored: true,
      tableName: 'UserHairPatterns',
    }
  );
  UserHairPatterns.associate = function (models) {
    
  };
  return UserHairPatterns;
};
