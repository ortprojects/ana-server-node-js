'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserRol = sequelize.define('UserRol', {
    rol: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    underscored: true,
    tableName: "UserRoles",
  });
  UserRol.associate = function (models) {
    models.UserRol.hasMany(models.User);
  };
  return UserRol;
};