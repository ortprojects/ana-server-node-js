'use strict';

module.exports = (sequelize, DataTypes) => {
  const UserHairTypes = sequelize.define(
    'UserHairTypes',
    {
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      hairTypeId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'HairTypes',
          key: 'id',
        },
      }
    },
    {
      underscored: true,
      tableName: 'UserHairTypes',
    }
  );
  UserHairTypes.associate = function (models) {
  };
  return UserHairTypes;
};
