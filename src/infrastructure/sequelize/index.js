'use strict';

import Sequelize from 'sequelize';
import configJson from '../config/configSequelize';

const env = process.env.NODE_ENV || 'development';
const config = configJson[env];
const db = {};

const sequelize = new Sequelize(config.database, config.username, config.password, config);

const modules = {
  Answer: require('./../models/answer'),
  HairPattern: require('./../models/hairpattern'),
  HairType: require('./../models/hairtype'),
  Ingredient: require('./../models/ingredient'),
  IngredientType: require('./../models/ingredienttype'),
  Newsletter: require('./../models/newsletter'),
  Post: require('./../models/post'),
  UserRol: require('./../models/userrol'),
  User: require('./../models/user'),
  UserHairTypes: require('./../models/userHairTypes'),
  UserHairPatterns: require('./../models/userHairPatterns')
};

Object.keys(modules).forEach((module) => {
  const model = modules[module](sequelize, Sequelize.DataTypes);
  db[module] = model;
});

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize.sync({ alter: true });
db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
