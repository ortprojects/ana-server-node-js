'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const promises = [];
    promises.push(queryInterface.bulkInsert('IngredientTypes', [{
      name: 'Prohibido',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('IngredientTypes', [{
      name: 'Permitido',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('IngredientTypes', [{
      name: 'No recomendado',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    return Promise.all(promises);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('IngredientTypes', null, {});
  }
};
