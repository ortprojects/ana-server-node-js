'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const promises = [];
    promises.push(queryInterface.bulkInsert('HairTypes', [{
      name: 'Porosidad Alta',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairTypes', [{
      name: 'Porosidad Media',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairTypes', [{
      name: 'Porosidad Baja',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    return Promise.all(promises);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('HairTypes', null, {});
  }
};
