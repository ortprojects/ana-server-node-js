'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const promises = [];
    promises.push(queryInterface.bulkInsert('UserRoles', [{
      rol: 'Administrador',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('UserRoles', [{
      rol: 'Moderador',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('UserRoles', [{
      rol: 'Registrado',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('UserRoles', [{
      rol: 'Invitado',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    return Promise.all(promises);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('UserRoles', null, {});
  }
};
