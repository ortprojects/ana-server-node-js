'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const promises = [];
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '1',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '2A',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '2B',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '2C',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '3A',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '3B',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '3C',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '4A',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '4B',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    promises.push(queryInterface.bulkInsert('HairPatterns', [{
      name: '4C',
      created_at: new Date(),
      updated_at: new Date()
    }], {}));
    return Promise.all(promises);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('HairPatterns', null, {});

  }
};
