import express from 'express';
import restRouter from './routes/index.js';
import cors from 'cors';
import { API_PATH_BASE } from './config/constants.js';

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(API_PATH_BASE, restRouter);

export default app;
