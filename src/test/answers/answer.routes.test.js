import request from 'supertest';
import { expect } from 'chai';
import app from '../../app';
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE;

describe('Answer routes that should be OK', function () {

    this.timeout(10000);

    let answerCreated = {};

    describe('POST /answer', function () {
        it('This should create an answer', function (done) {
            request(app)
            .post(`${apiPath}/answers`)
            .send({
                answer: {
                    userId: 1,
                    postId: 1,
                    description: "Example of an answer"
                }
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                answerCreated = res.body.data
                done();
            });
        });
    });

    describe('GET /answers', function () {
        it('This should gets all answers', function (done) {
            request(app)
            .get(`${apiPath}/answers`)
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                done();
            });
        });
    });

    describe('GET /answers by ID', function () {
        it('This should get an answer', function (done) {
            request(app)
            .get(`${apiPath}/answers/${answerCreated.id}`)
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.data.id).to.eq(answerCreated.id)
                done();
            });
        });
    });

    describe('PUT /answer', function () {
        it('This should update an answer', function (done) {
            request(app)
            .put(`${apiPath}/answers`)
            .send({
                answer: {
                    id: answerCreated.id, 
                    userId: answerCreated.userId,
                    postId: answerCreated.post,
                    description: "Test updated"
                }
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.message).to.eq("Answer updated successfully")
                done();
            });
        });
    });

    describe('DELETE /answer', function () {
        it('This should delete an answer', function (done) {
            request(app)
            .delete(`${apiPath}/answers/${answerCreated.id}`)
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.message).to.eq("Answer deleted successfully")
                done();
            });
        });
    });

});

describe('Answer routes that should fail', function () {
    describe('GET /answers by ID', function () {
        it('This should fail by null ID.', function (done) {
            request(app)
                .get(`${apiPath}/answers/-1`)
                .expect(404)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });
});