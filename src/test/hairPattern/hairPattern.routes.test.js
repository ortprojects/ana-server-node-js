import request from 'supertest';
import { expect } from 'chai';
import app from '../../app';
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE

describe('Hair pattern routes that should be OK', function () {

    describe('GET /hairPattern', function () {
        it('This should get all hair patterns', function (done) {
            request(app)
                .get(`${apiPath}/hair-patterns`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe('GET /hairPattern by ID', function () {
        it('This should get one hair pattern', function (done) {
            request(app)
                .get(`${apiPath}/hair-patterns/1`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.data.id).to.eq(1)
                    done();
                });
        });
    });
});

describe('Hair pattern routes that should fail', function () {
    describe('GET /hairPattern by ID', function () {
        it('This should fail by null ID.', function (done) {
            request(app)
                .get(`${apiPath}/hair-patterns/-1`)
                .expect(404)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });
});