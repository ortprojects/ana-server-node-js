import request from 'supertest';
import { expect } from 'chai';
import app from '../../app';
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE

var ingredientCreated = {};

describe('Ingredient routes that should be OK', function () {

    describe('POST /ingredient', function () {

        it('This should create an ingredient', function (done) {
            request(app)
                .post(`${apiPath}/ingredients`)
                .send({
                    ingredient: {
                        name: "PROBANDO NUEVO METODO",
                        description: "PROBANDO NUEVO METODO",
                        userId: 1,
                        ingredientTypeId: 1
                    }
                })
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    ingredientCreated = res.body.data
                    done();
                });
        });
    });

    describe('GET /ingredients', function () {
        it('This should get all ingredients', function (done) {
            request(app)
                .get(`${apiPath}/ingredients`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe('GET /ingredients by ID', function () {
        it('This should get one ingredient', function (done) {
            request(app)
                .get(`${apiPath}/ingredients/${ingredientCreated.id}`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.data.id).to.eq(ingredientCreated.id)
                    done();
                });
        });
    });

    describe('PUT /ingredient', function () {
        it('This should update an ingredient', function (done) {
            request(app)
                .put(`${apiPath}/ingredients`)
                .send({
                    ingredient: {
                        id: ingredientCreated.id,
                        name: "CAMBIANDO POR TEST",
                        description: ingredientCreated.description,
                        userId: ingredientCreated.userId,
                        ingredientTypeId: ingredientCreated.ingredientTypeId
                    }
                })
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.message).to.eq("Ingredient updated successfully")
                    done();
                });
        });
    });

    describe('DELETE /ingredient', function () {
        it('This should delete an ingredient', function (done) {
            request(app)
                .delete(`${apiPath}/ingredients/${ingredientCreated.id}`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.message).to.eq("Ingredient deleted successfully")
                    done();
                });
        });
    });

});

describe('Ingredient routes that should fail', function () {
    describe('GET /ingredients by ID', function () {
        it('This should fail by null ID.', function (done) {
            request(app)
                .get(`${apiPath}/ingredients/-1`)
                .expect(404)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });
});