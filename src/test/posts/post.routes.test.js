import request from 'supertest';
import { expect } from 'chai';
import app from '../../app';
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE;

describe('Post routes that should be OK', function () {

    let postCreated={};

    describe('POST /posts', function () {
        it('This should create a post', function (done) {
            request(app)
                .post(`${apiPath}/posts`)
                .send({
                    post: {
                        userId: 1,
                        title: "What is this ingredient about?",
                        description: "Example of a post"
                    }
                })
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    postCreated=res.body.data;
                    done();
                });
        });
    });

    describe('GET /posts', function () {
        it('This should gets all posts', function (done) {
            request(app)
                .get(`${apiPath}/posts`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe('GET /posts by ID', function () {
        it('This should get a post', function (done) {
            request(app)
                .get(`${apiPath}/posts/${postCreated.id}`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.data.id).to.eq(postCreated.id)
                    done();
                });
        });
    });

    describe('PUT /posts', function () {
        it('This should update a post', function (done) {
            request(app)
                .put(`${apiPath}/posts`)
                .send({
                    post: {
                        id:postCreated.id,
                        userId: postCreated.userId,
                        title: "What is Petrolato?",
                        description: "Its a not recommended ingredient"
                    }
                })
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe('DELETE /posts', function () {
        it('This should delete a post', function (done) {
            request(app)
                .delete(`${apiPath}/posts/${postCreated.id}`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

});

describe('Post routes that should fail', function () {
    describe('GET /posts by ID', function () {
        it('This should fail by null ID.', function (done) {
            request(app)
                .get(`${apiPath}/posts/-1`)
                .expect(404)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });
});