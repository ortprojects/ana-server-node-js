import request from 'supertest';
import { expect } from 'chai';
import app from '../../app'
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE



describe('UserRoles routes that should be OK', function () {

    describe('GET /user-roles', function () {
        it('This should get all user roles', function (done) {
            request(app)
                .get(`${apiPath}/user-roles`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });


});

