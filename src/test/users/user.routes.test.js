import request from 'supertest';
import { expect } from 'chai';
import app from '../../app';
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE;

describe('User routes that should be OK', function () {

    let userCreated = {};

    describe('POST /user', function () {
        it('should create an user', function (done) {
            request(app)
                .post(`${apiPath}/users`)
                .send({
                    user:
                    {
                        name: "ulrichNie",
                        email: "dark@gmail.com",
                        password: "timeemit",
                        avatarUrl: "https://pm1.narvii.com/6682/ed09bce60152a1c845420a3e92277a5975fbce95_00.jpg",
                        userRolId: 3
                    }
                })
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    userCreated = res.body.data;
                    done();
                });
        });
    });

    describe('GET /users', function () {
        it('should get all users', function (done) {
            request(app)
                .get(`${apiPath}/users`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe('GET /user by ID', function () {
        it('should get one user', function (done) {
            request(app)
                .get(`${apiPath}/users/${userCreated.id}`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.data.id).to.eq(userCreated.id)
                    done();
                });
        });
    });


    describe('PUT /user', function () {
        it('should update one user', function (done) {
            request(app)
                .put(`${apiPath}/users`)
                .send({
                    user: {
                        id: "1",
                        name: "Lince's96",
                        email: "wach@tar.inga",
                        password: "intcolect",
                        avatarUrl: "https://pbs.twimg.com/profile_images/1261741202115891200/PAZPSS57_400x400.jpg",
                        userRolId: 2
                    }
                })
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.message).to.eq("User updated successfully")
                    done();
                });
        });
    });    

    describe('DELETE /user by ID', function () {
        it('should delete one user', function (done) {
            request(app)
                .delete(`${apiPath}/users/${userCreated.id}`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.message).to.eq("User deleted successfully")
                    done();
                });
        });
    });
    
});

describe('User routes that should fail', function () {
    describe('GET /users by ID', function () {
        it('This should fail by null ID.', function (done) {
            request(app)
                .get(`${apiPath}/users/-1`)
                .expect(404)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });
});