import request from 'supertest';
import { expect } from 'chai';
import app from '../../app'
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE



describe('HairType routes that should be OK', function () {

    describe('GET /hair-types', function () {
        it('This should get all hair types', function (done) {
            request(app)
                .get(`${apiPath}/hair-types`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe('GET /hair-types by ID', function () {
        it('This should get one hair type', function (done) {
            request(app)
                .get(`${apiPath}/hair-types/1`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.data.id).to.eq(1)
                    done();
                });
        });
    });

});

