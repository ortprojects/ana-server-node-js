import request from 'supertest';
import { expect } from 'chai';
import app from '../../app';
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE;

describe('Newsletter routes that should be OK', function () {

    let newsletterCreated = {};

    describe('POST /newsletter', function () {
        it('This should create an newsletter', function (done) {
            request(app)
            .post(`${apiPath}/newsletters`)
            .send({
                newsletter: {
                    title: "Title of newsletter",
                    body: "Body of newsletter",
                    footer: "Footer of an answer"
                }
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                newsletterCreated = res.body.data
                done();
            });
        });
    });

    describe('GET /newsletter', function () {
        it('This should gets all newsletter', function (done) {
            request(app)
            .get(`${apiPath}/newsletters`)
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                done();
            });
        });
    });

    describe('GET /newsletter by ID', function () {
        it('This should get an answer', function (done) {
            request(app)
            .get(`${apiPath}/newsletters/${newsletterCreated.id}`)
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.data.id).to.eq(newsletterCreated.id)
                done();
            });
        });
    });

    describe('PUT /newsletter', function () {
        it('This should update an newsletter', function (done) {
            request(app)
            .put(`${apiPath}/newsletters`)
            .send({
                newsletter: {
                    id: newsletterCreated.id, 
                    title: "Title of newsletter updated",
                    body: "Body of newsletter updated",
                    footer: "Footer of an answer updated"
                }
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.message).to.eq("Newsletter updated successfully")
                done();
            });
        });
    });

    describe('DELETE /newsletter', function () {
        it('This should delete an answer', function (done) {
            request(app)
            .delete(`${apiPath}/newsletters/${newsletterCreated.id}`)
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.message).to.eq("Newsletter deleted successfully")
                done();
            });
        });
    });

});

describe('Newsletter routes that should fail', function () {
    describe('GET /newsletter by ID', function () {
        it('This should fail by null ID.', function (done) {
            request(app)
                .get(`${apiPath}/newsletters/-1`)
                .expect(404)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });
});