import request from 'supertest';
import { expect } from 'chai';
import app from '../../app'
import { API_PATH_BASE } from '../../config/constants';

const apiPath = API_PATH_BASE



describe('IngredientType routes that should be OK', function () {

    describe('GET /ingredient-types', function () {
        it('This should get all ingredient-types', function (done) {
            request(app)
                .get(`${apiPath}/ingredient-types`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe('GET /ingredient-types by ID', function () {
        it('This should get one ingredient', function (done) {
            request(app)
                .get(`${apiPath}/ingredient-types/1`)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.data.id).to.eq(1)
                    done();
                });
        });
    });

});

describe('IngredientType routes that should fail', function () {
    describe('GET /ingredient-types by ID', function () {
        it('This should fail by null ID.', function (done) {
            request(app)
                .get(`${apiPath}/ingredient-types/-1`)
                .expect(404)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });
});