import answerService from '../../services/answers';
import isInt from 'validator/lib/isInt';

const answerRoutes = {
  /**
   * This method gets all answers
   * @param {*} req
   * @param {*} res
   */
  async fetchAnswers(req, res) {
    try {
      const answers = await answerService.fetchAnswers();
      return res.status(200).send({
        message: '',
        data: answers,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },

  /**
   * This method get an answer by ID
   * @param {*} req
   * @param {*} res
   */
  async fetchAnswerById(req, res) {
    const { answerId } = req.params;

    if (answerId == undefined) {
      return res.status(404).send({
        message: 'Answer Id is not defined',
        data: [],
      });
    }

    if (answerId <= 0) {
      return res.status(404).send({
        message: 'Answer Id invalid',
        data: [],
      });
    }

    if (!isInt(answerId)) {
      return res.status(404).send({
        message: "Answer Id can't be a text",
        data: [],
      });
    }

    try {
      const answer = await answerService.fetchAnswerById(answerId);
      return res.status(200).send({
        message: '',
        data: answer,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },

  /**
   * This method creates an answer
   * @param {*} req
   * @param {*} res
   */
  async createAnswer(req, res) {
    const { answer } = req.body;
    
    if (answer == undefined) {
      return res.status(404).send({
        message: 'Answer is not defined',
        data: [],
      });
    }

    try {
      const answerCreated = await answerService.createAnswer(answer);
      return res.status(200).send({
        message: 'Answer created successfully',
        data: answerCreated,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },

  /**
   * This method deletes an answer
   * @param {*} req
   * @param {*} res
   */
  async deleteAnswer(req, res) {
    const { answerId } = req.params;

    if (answerId == undefined) {
      return res.status(404).send({
        message: 'Answer Id is not defined',
        data: [],
      });
    }

    if (answerId <= 0) {
      return res.status(404).send({
        message: 'Answer Id invalid',
        data: [],
      });
    }

    if (!isInt(answerId)) {
      return res.status(404).send({
        message: "Answer Id can't be a text",
        data: [],
      });
    }

    try {
      await answerService.deleteAnswer(answerId);

      return res.status(200).send({
        message: 'Answer deleted successfully',
        data: [],
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },

  /**
   * This method updates an answer
   * @param {*} req
   * @param {*} res
   */
  async updateAnswer(req, res) {
    const { answer } = req.body;

    if (answer == undefined) {
      return res.status(404).send({
        message: 'Answer is not defined',
        data: [],
      });
    }

    try {
      await answerService.updateAnswer(answer);
      return res.status(200).send({
        message: 'Answer updated successfully',
        data: [],
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },
};

export default answerRoutes;
