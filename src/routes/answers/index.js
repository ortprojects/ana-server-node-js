import express from 'express';
import answerRoutes from './answer.routes';

const answerRouter = express.Router();
 
answerRouter.route('/').get(answerRoutes.fetchAnswers);
answerRouter.route('/:answerId').get(answerRoutes.fetchAnswerById);
answerRouter.route('/').post(answerRoutes.createAnswer);
answerRouter.route('/:answerId').delete(answerRoutes.deleteAnswer);
answerRouter.route('/').put(answerRoutes.updateAnswer);
 
export default answerRouter;