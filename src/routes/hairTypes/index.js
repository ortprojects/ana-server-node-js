import express from 'express';
import hairTypeRoutes from './hairTypes.routes';

const hairTypeRouter = express.Router();

hairTypeRouter.route('/').get(hairTypeRoutes.fetchHairTypes);
hairTypeRouter.route('/:hairTypeId').get(hairTypeRoutes.fetchHairTypeById);

export default hairTypeRouter;