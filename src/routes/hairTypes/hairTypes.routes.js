import hairTypeService from "../../services/hairTypes";
import isInt from 'validator/lib/isInt';

const hairTypeRoutes = {

    /**
     * This method gets all hair types
     * @param {*} req
     * @param {*} res
     */
    async fetchHairTypes(req, res) {
        try {
            const hairTypes = await hairTypeService.fetchHairTypes();
            return res.status(200).send({
                message: "",
                data: hairTypes,
            });
        } catch (error) {
            return res.status(500).send({
                message: error.message,
                data:[],
            });
        }
    },

    /**
     * This method get an hair type by ID
     * @param {*} req
     * @param {*} res
     */
    async fetchHairTypeById(req, res) {
        const { hairTypeId } = req.params;

        if (hairTypeId == undefined) {
            return res.status(404).send({
              message: "Hair Type Id is not defined",
              data: [],
            });
          }
       
          if (hairTypeId <= 0) {
            return res.status(404).send({
              message: "Hair Type Id invalid",
              data: [],
            });
          }
       
          if (!isInt(hairTypeId)) {
            return res.status(404).send({
              message: "Hair Type Id can't be a text",
              data: [],
            });
          }

        try {
            const hairType = await hairTypeService.fetchHairTypeById(hairTypeId);
            return res.status(200).send({
                message: "",
                data: hairType,
            });
        } catch (error) {
            return res.status(500).send({
                message: error.message,
                data:[],
            });
        }
    },
};

export default hairTypeRoutes;