import express from 'express';
import newsletterRoutes from './newsletter.routes';

/**
 * Initialize and create routes
 */
const newsletterRouter = express.Router();

/**
 * TODO: implements all routes
 */

newsletterRouter.route('/').get(newsletterRoutes.fetchNewsletters);
newsletterRouter.route('/').post(newsletterRoutes.createNewsletter);
newsletterRouter.route('/:newsletterId').get(newsletterRoutes.fetchNewsletterById);
newsletterRouter.route('/:newsletterId').delete(newsletterRoutes.deleteNewsletterById);
newsletterRouter.route('/').put(newsletterRoutes.updateNewsletter);

export default newsletterRouter;
