import newsletterService from "../../services/newsletters";
import isInt from 'validator/lib/isInt';
import isEmpty from 'validator/lib/isEmpty';

const newsletterRoutes = {

    /**
     * Create newsletter and save in database
     * @param {*} req 
     * @param {*} res 
     */
    async createNewsletter(req, res) {
        const { newsletter } = req.body;

        if (isEmpty(newsletter.title)) {
            return res.status(404).send({
                message: "Newsletter Title is not defined",
                data: [],
            });
        }

        if (isEmpty(newsletter.body)) {
            return res.status(404).send({
                message: "Newsletter Body is not defined",
                data: [],
            });
        }

        if (isEmpty(newsletter.footer)) {
            return res.status(404).send({
                message: "Newsletter Footer is not defined",
                data: [],
            });
        }

        try {
            const newsletterCreated = await newsletterService.create(newsletter);

            return res.status(200).send({
                message: "Newsletter created successfully",
                data: newsletterCreated,
            });
        } catch (error) {
            console.log(error);

            return res.status(200).send({
                message: error.message,
                data: [],
            });
        }
    },

    /**
     * Fetch all newsletter by date and send to client
     * Query params = {date}
     * @param {*} req 
     * @param {*} res 
     */
    async fetchNewsletters(req, res) {
        try {
            const newsletters = await newsletterService.fetchNewsletters();

            return res.status(200).send({
                message: "",
                data: newsletters,
            });
        } catch (error) {
            console.log(error);

            return res.status(200).send({
                message: error.message,
                data: [],
            });
        }
    },

    /**
     * Delete newsletter by id
     * @param {*} req 
     * @param {*} res 
     */
    async deleteNewsletterById(req, res) {
        const { newsletterId } = req.params;

        if (newsletterId == undefined) {
            return res.status(404).send({
                message: "Newsletter Id is not defined",
                data: [],
            });
        }

        if (newsletterId <= 0) {
            return res.status(404).send({
                message: "Newsletter Id invalid",
                data: [],
            });
        }

        if (!isInt(newsletterId)) {
            return res.status(404).send({
                message: "Newsletter Id can't be a text",
                data: [],
            });
        }

        try {
            await newsletterService.deleteNewsletterById(newsletterId);

            return res.status(200).send({
                message: "Newsletter deleted successfully",
                data: [],
            });
        } catch (error) {
            console.log(error);

            return res.status(200).send({
                message: error.message,
                data: [],
            });
        }
    },

    /**
     *  Fetch newsletter by id
     * @param {*} req 
     * @param {*} res 
     */
    async fetchNewsletterById(req, res) {
        const { newsletterId } = req.params;

        if (newsletterId == undefined) {
            return res.status(404).send({
                message: "Newsletter is not defined",
                data: [],
            });
        }

        if (newsletterId <= 0) {
            return res.status(404).send({
                message: "Newsletter Id invalid",
                data: [],
            });
        }

        if (!isInt(newsletterId)) {
            return res.status(404).send({
                message: "Newsletter can't be a text",
                data: [],
            });
        }

        try {
            const newsletter = await newsletterService.fetchNewsletterById(newsletterId);
            return res.status(200).send({
                message: "",
                data: newsletter,
            });
        } catch (error) {
            return res.status(500).send({
                message: error.message,
                data: [],
            });
        }
    },

    /**
     * Update newsletter by id
     * @param {*} req 
     * @param {*} res 
     */
    async updateNewsletter(req, res) {
        const { newsletter } = req.body;

        if (newsletter == undefined) {
            return res.status(404).send({
                message: "Newsletter is not defined",
                data: [],
            });
        }

        try {
            await newsletterService.updateNewsletter(newsletter);

            return res.status(200).send({
                message: "Newsletter updated successfully",
                data: [],
            });
        } catch (error) {
            console.log(error);

            return res.status(200).send({
                message: error.message,
                data: [],
            });
        }
    }
};

export default newsletterRoutes;