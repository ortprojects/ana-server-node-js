import postService from "../../services/posts";
import isInt from 'validator/lib/isInt';

const postRoutes = {

  /**
   * Find all post.
   */
  async fetchPosts(req, res) {
    try {
      const posts = await postService.fetchPosts();
      return res.status(200).send({
        message: "",
        data: posts,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }

  },

  /**
  * Get a post by id.
   * @param {*} req
   * @param {*} res
   */
  async fetchPostById(req, res) {
    const { postId } = req.params;

    if (postId == undefined) {
      return res.status(404).send({
        message: "Post Id is not defined",
        data: [],
      });
    }

    if (postId <= 0) {
      return res.status(404).send({
        message: "Post Id invalid",
        data: [],
      });
    }

    if (!isInt(postId)) {
      return res.status(404).send({
        message: "Post Id can't be a text",
        data: [],
      });
    }

    try {
      const post = await postService.fetchPostById(postId);
      return res.status(200).send({
        message: "",
        data: post,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    };
  },


  /**
   * create a post.
   * @param {Object} req - This method receive the request from the client.
   * @param {Object} res - This method receive the response from de server.
   */
  async createPost(req, res) {
    const { post } = req.body;

    if (post == undefined) {
      return res.status(404).send({
        message: "Post is not defined",
        data: [],
      });
    }

    try {
      const postCreated = await postService.createPost(post);
      return res.status(200).send({
        message: "Post created successfully",
        data: postCreated,
      })
    } catch (error) {
      res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },

  /**
   * Update a post.
   * @param {Object} req - This method receive the request from the client.
   * @param {Object} res - This method receive the response from de server.
   */
  async update(req, res) {
    const { post } = req.body;

    if (post == undefined) {
      return res.status(404).send({
        message: "Post is not defined",
        data: [],
      });
    }

    try {
      await postService.update(post);
      return res.status(200).send({
        message: "Post updated successfully",
        data: [],
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }

  },

  /**
   * Delete a post.
   * @param {Object} req - This method receive the request from the client.
   * @param {Object} res - This method receive the response from de server.
   */
  async delete(req, res) {
    const { postId } = req.params;

    if (postId == undefined) {
      return res.status(404).send({
        message: "Post Id is not defined",
        data: [],
      });
    }

    if (postId <= 0) {
      return res.status(404).send({
        message: "Post Id invalid",
        data: [],
      });
    }

    if (!isInt(postId)) {
      return res.status(404).send({
        message: "Post Id can't be a text",
        data: [],
      });
    }

    try {
        await postService.delete(postId);
        return res.status(200).send({
          message: "Post deleted successfully",
          data: [],
        });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }

  },

};

export default postRoutes;

