import express from 'express';
import postRoutes from './post.routes';


 const postRouter = express.Router();


postRouter.route('/').get(postRoutes.fetchPosts);
postRouter.route('/:postId').get(postRoutes.fetchPostById);
postRouter.route('/').post(postRoutes.createPost);
postRouter.route('/').put(postRoutes.update);
postRouter.route('/:postId').delete(postRoutes.delete);





export default postRouter;
