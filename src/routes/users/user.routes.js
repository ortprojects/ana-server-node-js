import userService from '../../services/users';
import isEmail from 'validator/lib/isEmail';
import isEmpty from 'validator/lib/isEmpty';
import isInt from 'validator/lib/isInt';

const userRoutes = {
  /**
   * Creates user
   * @param {*} req
   * @param {*} res
   */
  async createUser(req, res) {
    const { user } = req.body;
    if (user == undefined) {
      return res.status(404).send({
        message: 'User is not defined',
        data: [],
      });
    }

    if (isEmpty(user.name)) {
      return res.status(404).send({
        message: "Name can't be empty",
        data: [],
      });
    }

    if (!isEmail(user.email)) {
      return res.status(404).send({
        message: 'Email is not valid',
        data: [],
      });
    }

    if (isEmpty(user.password)) {
      return res.status(404).send({
        message: "Password can't be empty",
        data: [],
      });
    }

    try {
      const userCreated = await userService.create(user);
      return res.status(200).send({
        message: 'User created successfully',
        data: userCreated,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },

  /**
   * Deletes given user
   * @param {*} req
   * @param {*} res
   */
  async deleteUser(req, res) {
    const { userId } = req.params;

    if (userId == undefined) {
      return res.status(404).send({
        message: 'User Id is not defined',
        data: [],
      });
    }

    if (userId <= 0) {
      return res.status(404).send({
        message: 'User Id invalid',
        data: [],
      });
    }

    if (!isInt(userId)) {
      return res.status(404).send({
        message: "User Id can't be a text",
        data: [],
      });
    }

    try {
      await userService.deleteUser(userId);

      return res.status(200).send({
        message: 'User deleted successfully',
        data: [],
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },
  /**
   * Gets all users
   * @param {*} req
   * @param {*} res
   */
  async fetchUsers(req, res) {
    try {
      const users = await userService.fetchUsers();
      res.status(200).send({
        message: '',
        data: users,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },
  /**
   * Gets user by ID
   * @param {*} req
   * @param {*} res
   */
  async fetchUserById(req, res) {
    const { userId } = req.params;

    if (userId == undefined) {
      return res.status(404).send({
        message: 'User Id is not defined',
        data: [],
      });
    }

    if (userId <= 0) {
      return res.status(404).send({
        message: 'User Id invalid',
        data: [],
      });
    }

    if (!isInt(userId)) {
      return res.status(404).send({
        message: "User Id can't be a text",
        data: [],
      });
    }

    try {
      const user = await userService.fetchUserById(userId);
      return res.status(200).send({
        message: '',
        data: user,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },

  async fetchUserByFirebaseUid(req, res) {
    const { firebaseUid } = req.params;

    if (firebaseUid == undefined) {
      return res.status(404).send({
        message: 'User Id is not defined',
        data: [],
      });
    }

    try {
      const user = await userService.fetchUserByFirebaseUid(firebaseUid);
      return res.status(200).send({
        message: '',
        data: user,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },
  /**
   * Updates given user
   * @param {*} req
   * @param {*} res
   */
  async updateUser(req, res) {
    const { user } = req.body;

    if (user == undefined) {
      return res.status(404).send({
        message: 'User is not defined',
        data: [],
      });
    }

    try {
      const userUpdated = await userService.updateUser(user);
      
      res.status(200).send({
        message: 'User updated successfully',
        data: userUpdated,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }
  },
};

export default userRoutes;
