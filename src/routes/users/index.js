import express from 'express';
import userRoutes from './user.routes';

/**
 * Initialize and create routes
 */
const userRouter = express.Router();

/**
 * TODO: implements all routes
 */

userRouter.route('/').get(userRoutes.fetchUsers);
userRouter.route('/').post(userRoutes.createUser);
userRouter.route('/:userId').delete(userRoutes.deleteUser);
userRouter.route('/firebase/:firebaseUid').get(userRoutes.fetchUserByFirebaseUid);
userRouter.route('/').put(userRoutes.updateUser);
userRouter.route('/:userId').get(userRoutes.fetchUserById);
export default userRouter;
