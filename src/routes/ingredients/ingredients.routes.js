import ingredientService from "../../services/ingredients";
import isInt from 'validator/lib/isInt';

const ingredientRoutes = {
  /**
   * Get all ingredients
   * @param {*} req
   * @param {*} res
   */
  async fetchIngredients(req, res) {
    try {
      const ingredients = await ingredientService.fetchIngredients();
      return res.status(200).send({
        message: "",
        data: ingredients,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: []
      })
    }
  },
    /**
   * Get all ingredients
   * @param {*} req
   * @param {*} res
   */
  async fetchIngredientsByList(req, res) {
    const { searchBy } = req.query;
    
    try {
      const ingredients = await ingredientService.fetchIngredientsByList(searchBy);
      return res.status(200).send({
        message: "",
        data: ingredients,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: []
      })
    }
  },
  /**
   * Get an ingredient by id.
   * @param {*} req
   * @param {*} res
   */
  async fetchIngredientById(req, res) {
    const { ingredientId } = req.params;

    if (ingredientId == undefined) {
      return res.status(404).send({
        message: "Ingredient Id is not defined",
        data: [],
      });
    }

    if (ingredientId <= 0) {
      return res.status(404).send({
        message: "Ingredient Id invalid",
        data: [],
      });
    }

    if (!isInt(ingredientId)) {
      return res.status(404).send({
        message: "Ingredient Id can't be a text",
        data: [],
      });
    }

    try {
      const ingredient = await ingredientService.fetchIngredientById(ingredientId);
      return res.status(200).send({
        message: "",
        data: ingredient,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: []
      })
    }
  },
  /**
  * Delete an ingredient.
  * @param {*} req
  * @param {*} res
  */
  async deleteIngredient(req, res) {
    const { ingredientId } = req.params;

    if (ingredientId == undefined) {
      return res.status(404).send({
        message: "Ingredient Id is not defined",
        data: [],
      });
    }

    if (ingredientId <= 0) {
      return res.status(404).send({
        message: "Ingredient Id invalid",
        data: [],
      });
    }

    if (!isInt(ingredientId)) {
      return res.status(404).send({
        message: "Ingredient Id can't be a text",
        data: [],
      });
    }

    try {
        await ingredientService.deleteIngredient(ingredientId);
        return res.status(200).send({
          message: "Ingredient deleted successfully",
          data: [],
        });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: []
      })
    }
  },
  /**
 * Update an ingrdient by id.
 * @param {*} req
 * @param {*} res
 */
  async updateIngredient(req, res) {
    const { ingredient } = req.body;

    if (ingredient == undefined) {
      return res.status(404).send({
        message: "Ingredient is not defined",
        data: [],
      });
    }

    try {
      await ingredientService.updateIngredient(ingredient);
      return res.status(200).send({
        message: "Ingredient updated successfully",
        data: [],
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: []
      })
    }


  },
  /**
   * Update an ingredient by id.
   * @param {*} req
   * @param {*} res
   */
  async createIngredient(req, res) {
    const { ingredient } = req.body;

    if (ingredient == undefined) {
      return res.status(404).send({
        message: "Ingredient is not defined",
        data: [],
      });
    }

    try {
      const ingredientCreated = await ingredientService.createIngredient(ingredient);
      return res.status(200).send({
        message: "Ingredient created successfully",
        data: ingredientCreated
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: []
        ,
      });
    }

  },
};

export default ingredientRoutes;