import express from 'express';
import ingredientRoutes from './ingredients.routes';

/**
 * Initialize and create routes
 */
const ingredientRouter = express.Router();

/**
 * TODO: implements all routes
 */
ingredientRouter.route('/').get(ingredientRoutes.fetchIngredients);
ingredientRouter.route('/filter?').get(ingredientRoutes.fetchIngredientsByList);
ingredientRouter.route('/:ingredientId').get(ingredientRoutes.fetchIngredientById);
ingredientRouter.route('/').post(ingredientRoutes.createIngredient);
ingredientRouter.route('/:ingredientId').delete(ingredientRoutes.deleteIngredient);
ingredientRouter.route('/').put(ingredientRoutes.updateIngredient);

 
export default ingredientRouter;
