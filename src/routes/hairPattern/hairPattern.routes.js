import hairPatternService from "../../services/hairPattern";
import isInt from 'validator/lib/isInt';

const hairPatternRoutes = {
    /**
     * Get all hair patterns
     * @param {*} req
     * @param {*} res
     */
    async fetchHairPattern(req, res) {
        try {
            const hairPattern = await hairPatternService.fetchHairPattern();
            return res.status(200).send({
                message: "",
                data: hairPattern,
            });
        } catch (error) {
            return res.status(500).send({
                message: error.message,
                data: []
            })
        }
    },
    /**
     * Get a hair pattern by id.
     * @param {*} req
     * @param {*} res
     */
    async fetchHairPatternById(req, res) {
        const { hairPatternId } = req.params;

        if (hairPatternId == undefined) {
            return res.status(404).send({
              message: "Hair Pattern Id is not defined",
              data: [],
            });
          }
       
          if (hairPatternId <= 0) {
            return res.status(404).send({
              message: "Hair Pattern Id invalid",
              data: [],
            });
          }
       
          if (!isInt(hairPatternId)) {
            return res.status(404).send({
              message: "Hair Pattern Id can't be a text",
              data: [],
            });
          }

        try {
            const hairPattern = await hairPatternService.fetchHairPatternById(hairPatternId);
            return res.status(200).send({
                message: "",
                data: hairPattern,
            });
        } catch (error) {
            return res.status(500).send({
                message: error.message,
                data: []
            })
        }
    },
};

export default hairPatternRoutes;