import express from 'express';
import hairPatternRoutes from './hairPattern.routes';

const hairPatternRouter = express.Router();

hairPatternRouter.route('/').get(hairPatternRoutes.fetchHairPattern);
hairPatternRouter.route('/:hairPatternId').get(hairPatternRoutes.fetchHairPatternById);

export default hairPatternRouter;