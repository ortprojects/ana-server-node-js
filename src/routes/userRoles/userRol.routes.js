import userRolService from "../../services/userRoles";

const userRolRoutes = {
    /**
     * Fetch all user roles
     * @param {*} req 
     * @param {*} res 
     */
    async fetchUserRoles(req, res) {
        try {
            const userRoles = await userRolService.fetchUserRoles();

            return res.status(200).send({
                message: "",
                data: userRoles,
            });
        } catch (error) {
            console.log(error);

            return res.status(200).send({
                message: error.message,
                data: [],
            });
        }
    },
};

export default userRolRoutes;