import express from 'express';
import userRolRoutes from './userRol.routes';

/**
 * Initialize and create routes
 */
const userRolRouter = express.Router();

userRolRouter.route('/').get(userRolRoutes.fetchUserRoles);

export default userRolRouter;
