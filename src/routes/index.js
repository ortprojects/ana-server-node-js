import express from 'express';
import userRouter from './users';
import postRouter from './posts';
import ingredientRouter from './ingredients';
import answersRouter from './answers';
import newsletterRouter from './newsletters';
import hairTypeRouter from './hairTypes';
import userRolRouter from './userRoles';
import hairPatternRouter from './hairPattern';
import ingredientTypeRouter from './ingredientType';

const restRouter = express.Router();

restRouter.use('/users', userRouter);
restRouter.use('/posts', postRouter);
restRouter.use('/ingredients', ingredientRouter);
restRouter.use('/answers', answersRouter);
restRouter.use('/newsletters', newsletterRouter);
restRouter.use('/hair-types', hairTypeRouter);
restRouter.use('/user-roles', userRolRouter);
restRouter.use('/hair-patterns', hairPatternRouter);
restRouter.use('/ingredient-types', ingredientTypeRouter);

export default restRouter;