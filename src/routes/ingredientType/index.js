import express from 'express';
import ingredientType from './ingredientType.routes';

/**
 * Initialize and create routes
 */
const ingredientTypeRouter = express.Router();

/**
 * TODO: implements all routes
 */
ingredientTypeRouter.route('/').get(ingredientType.fetchIngredientsTypes);
ingredientTypeRouter.route('/:ingredientTypeId').get(ingredientType.fetchIngredientTypeById);


 
export default ingredientTypeRouter;
