import ingredientTypeService from "../../services/ingredientType";
import isInt from 'validator/lib/isInt';

const ingredientTypeRoutes = {
  /**
   * Get all ingredients types
   * @param {*} req
   * @param {*} res
   */
  async fetchIngredientsTypes(req, res) {
    try {
      const ingredientsTypes = await ingredientTypeService.fetchIngredientsTypes();
      return res.status(200).send({
        message: "",
        data: ingredientsTypes,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    }

  },
  /**
   * Get an ingredient by id.
   * @param {*} req
   * @param {*} res
   */
  async fetchIngredientTypeById(req, res) {
    const { ingredientTypeId } = req.params;

    if (ingredientTypeId == undefined) {
      return res.status(404).send({
        message: "Ingredient Type Id is not defined",
        data: [],
      });
    }
 
    if (ingredientTypeId <= 0) {
      return res.status(404).send({
        message: "Ingredient Type Id invalid",
        data: [],
      });
    }
 
    if (!isInt(ingredientTypeId)) {
      return res.status(404).send({
        message: "Ingredient Type Id can't be a text",
        data: [],
      });
    }

    try {
      const ingredientType = await ingredientTypeService.fetchIngredientTypeById(ingredientTypeId);
      return res.status(200).send({
        message: "",
        data: ingredientType,
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: [],
      });
    };
  }

};

export default ingredientTypeRoutes;