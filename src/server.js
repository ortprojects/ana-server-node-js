import app from "./app.js";
import { PORT } from "./config/constants.js";
import db from "./infrastructure/sequelize/index.js";

async function main() {
  try {
    await db.sequelize.authenticate();
    console.log("Connection db has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }

  app.listen(PORT, function () {
    console.log(`Listening on port ${PORT}`);
  });
}

main();
