import db from '../../infrastructure/sequelize/index';

const Post = db.Post;
const User = db.User;
const Answer = db.Answer;

const postService = {
  /**
   * Gets all posts with no params
   * @returns {Promise<Post[]>} promise - all fetched posts
   */
  fetchPosts() {
    const promise = Post.findAll({
      include: [
        {
          model: User,
          as: 'user',
        },
      ],
    });

    return promise;
  },

  /**
   * Get a post type by id.
   * @param {number} postId - The id of the post .
   * @returns {Promise<Post>} promise - The searched post.
   */

  fetchPostById(postId) {
    const promise = Post.findByPk(postId, {
      include: [
        {
          model: User,
          as: 'user',
        },
        {
          model: Answer,
          as: 'answers',
          include: [{ model: User, as: 'user' }],
        },
      ],
    });

    return promise;
  },

  /**
   * Creates post
   * @param {Post} post - the post to be created
   * @return {Promise<Post>} promise - the created post
   */
  createPost(post) {
    const promise = Post.create(post);

    return promise;
  },

  /**
   * Updates post
   * @param {Post} post - the post to be updated
   * @returns {Promise<number[]>} promise -
   * first element equals number of affected rows,
   * second element equals the affected rows
   */
  update(post) {
    const promise = Post.update(post, {
      where: {
        id: post.id,
      },
    });
    return promise;
  },

  /**
   * Deletes post by ID
   * @param {number} postId - id of the post to be deleted
   * @returns {Promise<Number>} promise - the number of the deleted row
   */
  async delete(postId) {
    const post = await this.fetchPostById(postId);

    if (!post) {
      throw new Error('There is no post for that Id');
    }

    const promise = post.destroy();

    return promise;
  },
};

export default postService;
