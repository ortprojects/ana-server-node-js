import db from '../../infrastructure/sequelize/index';

const Ingredient = db.Ingredient;
const User = db.User;
const IngredientType = db.IngredientType;
const Op = db.Sequelize.Op;
const ingredientService = {
  /**
   * Get all ingredients with no params
   * @returns {Promise<Ingredient[]>} promise - all fetched ingredients.
   */
  fetchIngredients() {
    const promise = Ingredient.findAll({
      include: [
        {
          model: User,
          as: 'user',
        },
        {
          model: IngredientType,
          as: 'ingredientType',
        },
      ],
    });
    return promise;
  },
    /**
   * Get all ingredients with no params
   * @returns {Promise<Ingredient[]>} promise - all fetched ingredients.
   */
  fetchIngredientsByList(ingredients) {
    const promise = Ingredient.findAll({
      include: [
        {
          model: User,
          as: 'user',
        },
        {
          model: IngredientType,
          as: 'ingredientType',
        },
      ],
      where: {
        name: {
          [Op.or]: ingredients
        }
      }
       });
    return promise;
  },
  /**
   * Gets ingredient by ID
   * @param {number} ingredientId - id of the ingredient to fetch
   * @returns {Promise<Ingredient>} promise - the fetched ingredient
   */
  fetchIngredientById(ingredientId) {
    const promise = Ingredient.findByPk(ingredientId, {
      include: [
        {
          model: User,
          as: 'user',
        },
        {
          model: IngredientType,
          as: 'ingredientType',
        },
      ],
    });

    return promise;
  },
  /**
   * Deletes ingredient by ID
   * @param {number} ingredientId - id of the ingredient to be deleted
   * @returns {Promise<Ingredient>} promise - the number of the deleted row
   */
  async deleteIngredient(ingredientId) {
    const ingredient = await this.fetchIngredientById(ingredientId);

    if (!ingredient) {
      throw new Error('There is no ingredient for that Id');
    }

    const promise = ingredient.destroy();

    return promise;
  },
  /**
   * Updates ingredient
   * @param {Ingredient} ingredient - the ingredient to be updated
   * @returns {Promise<number[]>} promise -
   * first element equals number of affected rows,
   * second element equals the affected rows
   */
  async updateIngredient(ingredient) {
    try {
      const ingredientRet = await Ingredient.findByPk(ingredient.id, {
        include: [
          {
            model: IngredientType,
            as: 'ingredientType',
          },
        ],
      });
      ingredientRet.ingredientTypeId = ingredient.ingredientType.id;
      ingredientRet.name = ingredient.name;
      ingredientRet.description = ingredient.description;

      return ingredientRet.save();
    } catch (error) {
      throw new Error(error);
    }
  },
  /**
   * Creates ingredient.
   * @param {Ingredient} ingredient - the ingredient to be created
   * @returns {Promise<Ingredient>} promise - the created ingredient
   */
  createIngredient(ingredient) {
    const promise = Ingredient.create(ingredient);
    return promise;
  },
};
export default ingredientService;
