import db from '../../infrastructure/sequelize/index';

const UserRol = db.UserRol;

const userRolService = {
    /**
    * Fetches user roles
    * @returns {Promise<UserRol[]>} promise - all user roles
    */
    fetchUserRoles() {
        const promise = UserRol.findAll();
        return promise;
    },
}
export default userRolService;
