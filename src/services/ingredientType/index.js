import db from '../../infrastructure/sequelize/index';

const IngredientType = db.IngredientType;

const ingredientTypeService = {
    /**
    * Get all ingredients types.
    * @returns {Promise<IngredientType>} promise - The list of ingredients types.
   */
    fetchIngredientsTypes() {
        const promise = IngredientType.findAll();

        return promise;

    },
    /**
     * Get an ingredient type by id.
    * @param {number} ingredientId - The id of the ingredient type.
    * @returns {Promise<Ingredient>} promise - The search ingredient type.
   */
    fetchIngredientTypeById(ingredientTypeId) {
    const promise = IngredientType.findByPk(ingredientTypeId);

    return promise;
},
 


}
export default ingredientTypeService;