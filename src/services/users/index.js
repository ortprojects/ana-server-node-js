import db from '../../infrastructure/sequelize/index';

const User = db.User;
const UserRol = db.UserRol;
const UserHairPatterns = db.UserHairPatterns;
const UserHairTypes = db.UserHairTypes;
const HairPattern = db.HairPattern;
const HairType = db.HairType;

const userService = {
  /**
   * Creates user
   * @param {User} user - the user to create
   * @returns {Promise<User>} promise - the created user
   */
  async create(user) {
    let newUser;
    const transaction = db.sequelize.transaction(async (t) => {
      newUser = await User.create(user, { transaction: t });

      var promises = [];

      user.hairPatternIds.forEach((hairPatternId) => {
        const userHairPatternPromise = UserHairPatterns.create(
          {
            userId: newUser.id,
            hairPatternId: hairPatternId,
          },
          { transaction: t }
        );

        promises.push(userHairPatternPromise);
      });

      user.hairTypeIds.forEach((hairTypeId) => {
        const userHairTypePromise = UserHairTypes.create(
          {
            userId: newUser.id,
            hairTypeId: hairTypeId,
          },
          { transaction: t }
        );

        promises.push(userHairTypePromise);
      });

      return Promise.all(promises);
    });

    await transaction;

    const userById = this.fetchUserById(newUser.id);

    return userById;
  },

  /**
   * Deletes user by ID
   * @param {number} id - id of the user to be deleted
   * @returns {Promise<Number>} promise - the number of the deleted row
   */
  async deleteUser(id) {
    const user = await this.fetchUserById(id);

    if (!user) {
      throw new Error('There is no user for that Id');
    }

    const promise = user.destroy();

    return promise;
  },
  /**
   * Gets all users with no params
   * Gets all users
   * @returns {Promise<User[]>} promise - all fetched users
   * @returns {Promise<User[]>} promise - all users
   */
  fetchUsers() {
    const promise = User.findAll({
      include: [
        {
          model: UserRol,
          as: 'userRol',
        },
      ],
    });
    return promise;
  },
  /**
   * Gets user by ID
   * @param {number} id - id of the user to be fetched
   * @returns {Promise<User>} promise - the fetched user
   */
  fetchUserById(id) {
    const promise = User.findByPk(id, {
      include: [
        {
          model: UserRol,
          as: 'userRol',
        },
        {
          model: HairPattern,
          as: 'hairPatterns',
        },
        {
          model: HairType,
          as: 'hairTypes',
        },
      ],
    });
    return promise;
  },

  /**
   * Gets user by firebaseUid
   * @param {string} firebaseUid - firebaseUid of the user to be fetched
   * @returns {Promise<User>} promise - the fetched user
   */
  async fetchUserByFirebaseUid(firebaseUid) {
    const user = await User.findOne({
      where: {
        firebaseUid,
      },
    });

    return this.fetchUserById(user.id);
  },

  /**
   * Updates user
   * @param {User} user - the user to update
   * @returns {Promise<number[]>} promise -
   * first element equals number of affected rows,
   * second element equals the affected rows
   */
  async updateUser(user) {
    try {
      const userRet = await User.findByPk(user.id, {
        include: [
          {
            model: UserRol,
            as: 'userRol',
          },
          {
            model: HairPattern,
            as: 'hairPatterns',
          },
          {
            model: HairType,
            as: 'hairTypes',
          },
        ],
      });

      userRet.name = user.name;

      return userRet.save();
    } catch (error) {
      throw new Error(error);
    }
  },
};
export default userService;
