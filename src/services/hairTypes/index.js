import db from '../../infrastructure/sequelize/index';

const HairType = db.HairType;

const hairTypeService = {

    /**
     * Gets all hair types with no params
     * @returns {Promise<HairType[]>} promise - all fetched hair types
     */
    fetchHairTypes(){
        const promise = HairType.findAll();
        return promise;
    },

    /**
     * Gets hair type by ID
     * @param {number} id - id of the hair type to fetch
     * @returns {Promise<HairType>} promise - the fetched hair type
     */
    fetchHairTypeById(id){
        const promise = HairType.findByPk(id);
        return promise;
    },
};

export default hairTypeService;