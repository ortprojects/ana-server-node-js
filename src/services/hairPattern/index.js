import db from "../../infrastructure/sequelize/index"

const hairPattern = db.HairPattern;

const hairPatternService = {


  /**
  * Get all hair patterns.
  * @returns {Promise<HairPattern>} promise - The list of ingredients.
 */
  fetchHairPattern() {
    const promise = hairPattern.findAll();

    return promise;

  },
  /**
   * Get a hair pattern by id.
  * @param {number} hairPatternId - The id of the ingredient.
  * @returns {Promise<HairPattern>} promise - The deleted ingredient.
 */
  fetchHairPatternById(hairPatternId) {
    const promise = hairPattern.findByPk(hairPatternId);

    return promise;
  }
}
export default hairPatternService;