import db from '../../infrastructure/sequelize/index';

const UserHairPatterns = db.UserHairPatterns;

const userHairPatternsService = {
  create(userId, hairPatternId, options) {
    const userHairPattern = UserHairPatterns.create({
      userId,
      hairPatternId
    }, options);

    return userHairPattern;
  }
};

export default userHairPatternsService;