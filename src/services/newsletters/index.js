import db from "../../infrastructure/sequelize/index"

const Newsletter = db.Newsletter;

const newsletterService = {
    /**
    * Creates Newsletter
    * @param {Newsletter} newsletter - the newsletter to create
    * @returns {Promise<Newsletter>} promise - the created newsletter
    */
    create(newsletter) {
        const promise = Newsletter.create(newsletter);

        return promise;
    },

    /**
    * Deletes newsletter by ID
    * @param {number} newsletterId - id of the newsletter to be deleted
    * @returns {Promise<Number>} promise - the number of the deleted row
    */
    async deleteNewsletterById(newsletterId) {
        const newsletter = await this.fetchNewsletterById(newsletterId);

        if(!newsletter){
            throw new Error('There is no newsletter for that Id')
        }

        const promise = newsletter.destroy();

        return promise;
    },

    /**
    * Gets newsletter by date
    * @param {date} date - date of the newsletter to fetch
    * @returns {Promise<Newsletter[]>} promise - all newsletters of the provided date
    */
    fetchNewsletters() {
        const promise = Newsletter.findAll();

        return promise;
    },

    /**
    * Updates newsletter by id 
    * @param {Newsletter} newsletter - the newsletter to update
    * @returns {Promise<number[]>} promise - 
    * first element equals number of affected rows, 
    * second element equals the affected rows
    */
    updateNewsletter(newsletter) {
        const promise = Newsletter.update(newsletter, {
            where: {
                id: newsletter.id
            }
        });

        return promise;
    },

    /**
     * Get an newsletter by id.
     * @param {number} newsletterId - The id of the newslette.
     * @returns {Promise<Newsletter>} promise - The search newslette.
    */
    fetchNewsletterById(newsletterId) {
        const promise = Newsletter.findByPk(newsletterId);

        return promise;
    },
}

export default newsletterService;
