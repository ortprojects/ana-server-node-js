import db from '../../infrastructure/sequelize/index';

const Answer = db.Answer;
const User = db.User;
const Post = db.Post;

const answerService = {

    /**
     * Gets all answers with no params
     * @returns {Promise<Answer[]>} promise - all fetched answers
     */
    fetchAnswers() {
        const promise = Answer.findAll({
            include: [
                {
                    model: User,
                    as: "user"
                },
                {
                    model: Post,
                    as: "post"
                }
            ]
        });
        return promise;
    },

    /**
     * Gets answer by ID
     * @param {number} id - id of the anwswer to fetch
     * @returns {Promise<Answer>} promise - the fetched answer
     */
    fetchAnswerById(id) {
        const promise = Answer.findByPk(id, {
            include: [
                {
                    model: User,
                    as: "user"
                },
                {
                    model: Post,
                    as: "post"
                }
            ]
        });
        return promise;
    },

    /**
     * Creates answer
     * @param {Answer} answer - the answer to be created
     * @return {Promise<Answer>} promise - the created answer
     */
    createAnswer(answer) {
        const promise = Answer.create(answer);
        return promise;
    },

    /**
     * Updates answer
     * @param {Answer} answer - the answer to be updated
     * @returns {Promise<number[]>} promise - 
     * first element equals number of affected rows, 
     * second element equals the affected rows
     */
    updateAnswer(answer) {
        const promise = Answer.update(answer, {
            where: {
                id: answer.id
            }
        });
        return promise;
    },

    /**
     * Deletes answer
     * @param {answerId} answerId - the answer id to be deleted
     * @returns {Promise<Boolean>} promise - the number of the deleted row
     */
    async deleteAnswer(answerId) {
        const answer = await this.fetchAnswerById(answerId);

        if(!answer){
            throw new Error('There is no answer for that Id')
        }

        const promise = answer.destroy();

        return promise;
    }
};

export default answerService;