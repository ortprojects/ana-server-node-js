import db from '../../infrastructure/sequelize/index';

const UserHairTypes = db.UserHairTypes;

const userHairTypesService = {
  create(userId, hairTypeId, options) {
      console.log(userId)
    const userHairType = UserHairTypes.create({
      user_id,
      hairTypeId
    }, options);

    return userHairType;
  }
};

export default userHairTypesService;